<?php

namespace Weirdo\LaravelRequestDocs\Tests;

use Illuminate\Support\Facades\Route;
use Orchestra\Testbench\TestCase as Orchestra;
use Weirdo\LaravelRequestDocs\LaravelRequestDocsServiceProvider;
use Weirdo\LaravelRequestDocs\Tests\Stubs\TestControllers;

class TestCase extends Orchestra
{

    public function setUp(): void
    {
        parent::setUp();
        $this->registerRoutes();
    }

    protected function getPackageProviders($app)
    {
        return [
            LaravelRequestDocsServiceProvider::class,
        ];
    }

    public function getEnvironmentSetUp($app)
    {
        app()->setBasePath(__DIR__ . '/../');

        $app['config']->set('database.default', 'testing');
        $app['config']->set('app.debug', true);
    }

    public function registerRoutes()
    {
        Route::get('/', [TestControllers\WelcomeController::class, 'index']);
        Route::get('welcome', [TestControllers\WelcomeController::class, 'index']);
        Route::get('welcome/{id}', [TestControllers\WelcomeController::class, 'show']);
        Route::post('welcome', [TestControllers\WelcomeController::class, 'store'])->middleware('auth:api');
        Route::put('welcome', 'Weirdo\LaravelRequestDocs\Tests\Stubs\TestControllers\WelcomeController@edit');
        Route::patch('welcome/patch', 'Weirdo\LaravelRequestDocs\Tests\Stubs\TestControllers\WelcomeController@edit');
        Route::delete('welcome', [TestControllers\WelcomeController::class, 'destroy']);
        Route::get('health', [TestControllers\WelcomeController::class, 'health']);
        Route::get('single', TestControllers\SingleActionController::class);
        Route::delete('welcome/no-rules', [TestControllers\WelcomeController::class, 'noRules']);
        Route::post('comments-on-request-rules-method', [TestControllers\CommentsOnRequestRulesMethodController::class, 'index']);

        Route::get('closure', function () {
            return true;
        });

        Route::apiResource('accounts', TestControllers\AccountController::class);

        Route::match(['get', 'post'], 'match', [TestControllers\MatchController::class, 'index']);

        // Test duplication
        Route::apiResource('accounts', TestControllers\AccountController::class);

        // Expected to be skipped
        Route::get('telescope', [TestControllers\TelescopeController::class, 'index']);

        Route::options('options_is_not_included', function () {
            return false;
        });
    }

    protected function countRoutesWithLRDDoc(): int
    {
        return count(Route::getRoutes()->getRoutes()) - 2; // Exclude `telescope`, `request-docs`
    }
}
