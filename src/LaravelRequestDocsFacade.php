<?php

namespace Weirdo\LaravelRequestDocs;

use Illuminate\Support\Facades\Facade;

/**
 * @codeCoverageIgnore
 * @see \Weirdo\LaravelRequestDocs\LaravelRequestDocs
 */
class LaravelRequestDocsFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return LaravelRequestDocs::class;
    }
}
